'use strict';
module.exports = function(logger) {
    return {
        pre: function(message, callback) {
            logger.info(message.op, message.params);

            callback(null, message);
        },
        post: function(data, callback) {
            var message = data.error ?
                data.error.name + ': ' + data.error.message :
                data.result;

            logger.info(message);

            callback(null, data);
        }
    };
};
